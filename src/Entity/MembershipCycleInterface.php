<?php

namespace Drupal\membership_cycle\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Membership cycle entities.
 *
 * @ingroup membership_cycle
 */
interface MembershipCycleInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Membership cycle type.
   *
   * @return string
   *   The Membership cycle type.
   */
  public function getType();

  /**
   * Gets the Membership cycle name.
   *
   * @return string
   *   Name of the Membership cycle.
   */
  public function getName();

  /**
   * Sets the Membership cycle name.
   *
   * @param string $name
   *   The Membership cycle name.
   *
   * @return \Drupal\membership_cycle\Entity\MembershipCycleInterface
   *   The called Membership cycle entity.
   */
  public function setName($name);

  /**
   * Gets the Membership cycle creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Membership cycle.
   */
  public function getCreatedTime();

  /**
   * Sets the Membership cycle creation timestamp.
   *
   * @param int $timestamp
   *   The Membership cycle creation timestamp.
   *
   * @return \Drupal\membership_cycle\Entity\MembershipCycleInterface
   *   The called Membership cycle entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Membership cycle published status indicator.
   *
   * Unpublished Membership cycle are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Membership cycle is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Membership cycle.
   *
   * @param bool $published
   *   TRUE to set this Membership cycle to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\membership_cycle\Entity\MembershipCycleInterface
   *   The called Membership cycle entity.
   */
  public function setPublished($published);

}
