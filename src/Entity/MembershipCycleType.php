<?php

namespace Drupal\membership_cycle\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Membership cycle type entity.
 *
 * @ConfigEntityType(
 *   id = "membership_cycle_type",
 *   label = @Translation("Membership cycle type"),
 *   handlers = {
 *     "list_builder" = "Drupal\membership_cycle\MembershipCycleTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\membership_cycle\Form\MembershipCycleTypeForm",
 *       "edit" = "Drupal\membership_cycle\Form\MembershipCycleTypeForm",
 *       "delete" = "Drupal\membership_cycle\Form\MembershipCycleTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\membership_cycle\MembershipCycleTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "membership_cycle_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "membership_cycle",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/membership_cycle_type/{membership_cycle_type}",
 *     "add-form" = "/admin/structure/membership_cycle_type/add",
 *     "edit-form" = "/admin/structure/membership_cycle_type/{membership_cycle_type}/edit",
 *     "delete-form" = "/admin/structure/membership_cycle_type/{membership_cycle_type}/delete",
 *     "collection" = "/admin/structure/membership_cycle_type"
 *   }
 * )
 */
class MembershipCycleType extends ConfigEntityBundleBase implements MembershipCycleTypeInterface {

  /**
   * The Membership cycle type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Membership cycle type label.
   *
   * @var string
   */
  protected $label;

  public $description;

}
