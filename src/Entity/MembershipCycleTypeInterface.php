<?php

namespace Drupal\membership_cycle\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Membership cycle type entities.
 */
interface MembershipCycleTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
