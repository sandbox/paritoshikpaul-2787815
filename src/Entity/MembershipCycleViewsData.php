<?php

namespace Drupal\membership_cycle\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Membership cycle entities.
 */
class MembershipCycleViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['membership_cycle']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Membership cycle'),
      'help' => $this->t('The Membership cycle ID.'),
    );

    return $data;
  }

}
