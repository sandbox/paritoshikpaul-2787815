<?php

namespace Drupal\membership_cycle;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for membership_cycle.
 */
class MembershipCycleTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
