<?php
/**
 * @file
 * Contains \Drupal\example\Form\exampleSettingsForm
 */
namespace Drupal\membership_cycle\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class MembershipSettingsForm extends ConfigFormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'membership_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'membership.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('membership.settings');

        $form['membership_test'] = array(
            '#type' => 'textfield',
            '#title' => $this->t(''),
            '#default_value' => $config->get(''),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = \Drupal::service('config.factory')->getEditable('membership.settings');
        $config->set('things', $form_state->getValue('membership_test'))
            ->save();

        parent::submitForm($form, $form_state);
    }
}

?>