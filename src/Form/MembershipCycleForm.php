<?php

namespace Drupal\membership_cycle\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Membership cycle edit forms.
 *
 * @ingroup membership_cycle
 */
class MembershipCycleForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\membership_cycle\Entity\MembershipCycle */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Membership cycle.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Membership cycle.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.membership_cycle.canonical', ['membership_cycle' => $entity->id()]);
  }

}
