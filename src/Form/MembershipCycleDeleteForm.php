<?php

namespace Drupal\membership_cycle\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Membership cycle entities.
 *
 * @ingroup membership_cycle
 */
class MembershipCycleDeleteForm extends ContentEntityDeleteForm {


}
