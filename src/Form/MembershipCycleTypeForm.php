<?php

namespace Drupal\membership_cycle\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MembershipCycleTypeForm.
 *
 * @package Drupal\membership_cycle\Form
 */
class MembershipCycleTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $membership_cycle_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $membership_cycle_type->label(),
      '#description' => $this->t("Label for the Membership cycle type."),
      '#required' => TRUE,
    ];
      dpm($membership_cycle_type->description);
      $form['description'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Description'),
          '#maxlength' => 255,
          '#default_value' => $membership_cycle_type->description,
          '#description' => $this->t("Description for the Membership cycle type."),
          '#required' => TRUE,
      ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $membership_cycle_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\membership_cycle\Entity\MembershipCycleType::load',
      ],
      '#disabled' => !$membership_cycle_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $membership_cycle_type = $this->entity;
    $status = $membership_cycle_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Membership cycle type.', [
          '%label' => $membership_cycle_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Membership cycle type.', [
          '%label' => $membership_cycle_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($membership_cycle_type->urlInfo('collection'));
  }

}
