<?php

namespace Drupal\membership_cycle;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Membership cycle entity.
 *
 * @see \Drupal\membership_cycle\Entity\MembershipCycle.
 */
class MembershipCycleAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\membership_cycle\Entity\MembershipCycleInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished membership cycle entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published membership cycle entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit membership cycle entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete membership cycle entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add membership cycle entities');
  }

}
