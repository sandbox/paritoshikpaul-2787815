<?php

/**
 * @file
 * Contains membership_cycle.page.inc.
 *
 * Page callback for Membership cycle entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Membership cycle templates.
 *
 * Default template: membership_cycle.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_membership_cycle(array &$variables) {
  // Fetch MembershipCycle Entity Object.
  $membership_cycle = $variables['elements']['#membership_cycle'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
